"""
run sorted on the command line
"""
from lib import configure, filefinder

def main():
    """
    run the whole thing from the command line using params or the config.file
    """
    config = configure.Configure()
    config.validate()
    finder = filefinder.FileFinder(".")
    finder.find()


if __name__ == "__main__":
    main()
