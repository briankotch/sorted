'use strict';
module.exports = function(grunt) {
    grunt.initConfig({
        watch: {
            python: { 
                files: [
                    'lib/*.py',
                    '*.py' 
                ],
                tasks: ['build'],
                options: {
                  
                }
            }
        },
        pylint: {
            options: { 
                rcfile: '.pylintrc',
                disable: ['line-too-long', 'too-few-public-methods'],
            },
            dist: {
                src: ['lib/*.py', '*.py', 'tests/*.py']
            }
        },
        nose: { 
            options: { },
            dist: { src: ['tests'] }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-nose');
    grunt.loadNpmTasks('grunt-pylint');
    grunt.registerTask('build', ['pylint', 'nose']);
    grunt.registerTask('default', ['build']);
};
