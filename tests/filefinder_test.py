""" unit tests for the file finder algorithim """
from lib import filefinder, constants, found_files
import os
def get_root():
    """ get the root directory of the test data relative to the test location """
    return "data"

def test_filefinder():
    """ make sure the instanstiated root matches the specified root """
    ffinder = filefinder.FileFinder(get_root())
    assert ffinder.root == get_root()
    ffinder.file_types = ['.txt']
    ffinder.find()
    print get_root()
    assert len(ffinder.found_files.data[constants.KEY_PATH]) == 3
    assert os.path.exists(ffinder.found_files.data_path)
    #test loading saved files
    print ffinder.found_files.data_file
    ffiles = found_files.FoundFiles(constants.DATA_LOCATION, ffinder.found_files.name)
    print ffiles.data_file
    print ffinder.found_files.data
    print ffiles.data
    assert len(ffiles.data[constants.KEY_PATH]) == 3

