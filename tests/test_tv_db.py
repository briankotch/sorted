""" unit tests for tv_db interactions """
from lib import tv_db

def test_getting_api_key():
    """ make sure the api key is registered in the environment """
    assert tv_db.get_tv_db_api_key() != ""

def test_getting_server_times():
    """ test getting the server time """
    tvdb_time = tv_db.get_current_server_time()
    assert len(tvdb_time) == 10
