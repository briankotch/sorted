""" unit tests for the astropy data layer """
from lib import data_layer
import os

def build_table_data():
    """ build a static test dictiomary """
    return {
        'a': [1, 2, 3],
        'b': [4, 5, 6],
        'c': [1, 2, 3],
    }

def test_data_dict():
    """ test the tables with a dict """
    dlayer = data_layer.Data("data", "test")
    dlayer.data = build_table_data()
    assert len(dlayer.data.keys()) == 3
    dlayer.write()
    assert os.path.exists(dlayer.data_path)
