""" unit tests for the configuration object """
from lib import configure
import os

def get_config_file():
    """ setup the expected config file """
    return os.path.join("data", "config.json")

def test_config_file_exists():
    """ make sure the test config file exists """
    config_file = get_config_file()
    assert os.path.isfile(config_file)

def test_config_file_valid():
    """ make sure this file validates """
    config_file = get_config_file()
    config = configure.Configure(config_file)
    assert config.validate()
