"""
Python doesn't have constants, there is nothing to stop anyone from changing these
"""
FILE_TYPES = ['.avi', '.txt']
DATA_LOCATION = "data"
KEY_DIR = "dir"
KEY_PATH = "path"
KEY_SEARCH = "search"
