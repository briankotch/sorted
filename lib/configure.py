"""
A class for reading, editing and saving the sorted configuration
"""
import json
import sys
import os

class Configure(object):
    """ sorteds configuration manager """
    def __init__(self, config_filename=None):
        self.json = None
        self.config_filename = None
        self.load_configuration_file(config_filename)

    def load_configuration_file(self, config_filename):
        """ reads the json from the filesystem """
        config_text = None
        if config_filename == None:
            self.config_filename = os.path.join(sys.path[0], "config.json")
        else:
            self.config_filename = config_filename
        if os.path.isfile(self.config_filename):
            config_text = open(self.config_filename).read()
            self.json = json.loads(config_text)
        else:
            message = "config file not found: %s" % (self.config_filename)
            raise Exception(message)

    def validate(self):
        """ validates the loaded configure file """
        if self.config_filename != None:
            return True
        else:
            return False
