""" an abstration of astropy's table for FITS """
import pickle
import os

class Data(object):
    """ load and save arbitrary tables to the fs """
    def __init__(self, data_path, name):
        self.data_path = data_path
        self.data_file = os.path.join(data_path, name)
        self.name = name
        self.data = {}
        self.load()

    def load(self):
        """ load a table file from the fs or create a new object """
        if os.path.isfile(self.data_file):
            print "Loading %s" % os.path.abspath(self.data_file)
            self.data = pickle.load(open(self.data_file, 'rb'))
        else:
            self.data = {}

    def write(self):
        """ write the table to the filesystem """
        if not os.path.exists(self.data_path):
            os.makedirs(self.data_path)
        print "Writing %s " % os.path.abspath(self.data_file)
        pickle.dump(self.data, open(self.data_file, "wb"))
