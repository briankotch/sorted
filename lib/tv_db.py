""" TV DB handler """
import os
import untangle
#from xml.dom import minidom

MIRROR_URL = "http://thetvdb.com/api/%s/mirrors.xml"
TIME_URL = "http://thetvdb.com/api/Updates.php?type=none"
UPDATES_URL = "http://thetvdb.com/api/Updates.php?type=all&time=%s"

def get_tv_db_api_key():
    """ get the api key registered in the environment """
    return os.environ['TV_DB_API_KEY']

def get_current_server_time():
    """ get the current server time from tvdb """
    response = untangle.parse(TIME_URL)
    return response.Items.Time.cdata

def get_all_series_episodes():
    """ get the full list of episodes """
    response = untangle.parse(UPDATES_URL % get_current_server_time())
    return response
