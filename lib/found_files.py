"""
a class for mapping file results
"""
from lib import data_layer
from lib import constants
class FoundFiles(data_layer.Data):
    """ a self-saving dictionary of file results """
    def __init__(self, data_path, name):
        super(FoundFiles, self).__init__(data_path, name)
        if self.data == {}:
            self.data = {
                #constants.KEY_DIR: [],
                constants.KEY_PATH: [],
            }
