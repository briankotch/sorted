"""
A class for walking the file system and finding files
"""
import os
import scandir
import datetime
from lib import constants, found_files
class FileFinder(object):
    """ a file mapper """
    def __init__(self, root):
        self.root = root
        self.file_types = constants.FILE_TYPES
        self.name = "%s.pickle" % "".join([constants.KEY_SEARCH, datetime.datetime.now().strftime("%y%m%d_%H%M%S")])
        self.found_files = found_files.FoundFiles(constants.DATA_LOCATION, self.name)

    def find(self):
        """
        Build a dictionary of the files we care about
        """
        for path, _, files in scandir.walk(self.root):
            for filename in files:
                file_ext = os.path.splitext(filename)[1]
                if file_ext in self.file_types:
                    file_path = os.path.abspath(os.path.join(path, filename))
                    self.found_files.data[constants.KEY_PATH].append(file_path)
        self.found_files.write()
